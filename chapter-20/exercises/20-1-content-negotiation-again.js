var http = require("http");
var contentTypes = ["text/plain", "text/html",
                    "application/json", "application/rainbows+unicorns"];

contentTypes.forEach(function(type) {
    getResponse(type);
});

function getResponse(contentType) {
  var request = http.request({
    hostname: "eloquentjavascript.net",
    path: "/author",
    method: "GET",
    headers: {Accept: contentType}
  },
  function(response) {
    if (response.statusCode !== 200) {
      console.error("Request for " + contentType + " failed: " + response.statusMessage);
      return;
    }
    readStreamAsString(response, function(error, content) {
      if (error) throw error;
      console.log("-------\nType " + contentType + ":\n-------\n" + content);
    });
  });
  request.end();
}

function readStreamAsString(stream, callback) {
  var data = "";
  stream.on("data", function(chunk) {
    data += chunk.toString();
  });
  stream.on("end", function() {
    callback(null, data);
  });
  stream.on("error", function(error) {
    callback(error);
  });
}


