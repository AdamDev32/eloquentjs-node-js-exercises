/*
 * Using a regular expression is trivial.
 * It also does not guarantee security.
 * The best way is to deny access at the permissions level on the server, 
 * not parse URLs to deny access to certain folders.
 *
 * Decided not to do this exercise because the premise is flawed.
 *
 */